<?php
      error_reporting(E_ALL);
      ini_set('display_errors', '1');

      $log_path = "log.txt";
      $last_import_timestamp_path = "last_koha_import.txt";
      $statusstr = file_get_contents($last_import_timestamp_path);
      if(serialize(false)!=$statusstr)$last_status = unserialize($statusstr);
      if(!isset($last_status)) $last_status = array();
      if(!isset($last_status['records'])) $last_status['records'] = array();
      if(!isset($last_status['repository'])) $last_status['repository']='';

      $last_import_repository = $last_status['repository'];
      $foldersloaded = glob('/home/nvli/vufindharvest-master/data/*', GLOB_ONLYDIR | GLOB_NOSORT);
      if(is_array($foldersloaded) && count($foldersloaded) > 1 && $last_import_repository!='') {
	      $repositories = arrayRotate($foldersloaded,$last_import_repository); 
      }
      else $repositories = $foldersloaded;
	
      foreach($repositories as $repository)
        {
            if(isset($last_status['recordCount'][$repository])) $recordCount =  $last_status['recordCount'][$repository]+1; else $recordCount = 0;
            $files = glob($repository.'/*xml');
            
             if (is_array($files)){
	       for($i = $recordCount;$i<min($recordCount+10,count($files));$i++){
		       global $retry;
		       $retry =0;
		       $XMLfilename_raw = $files[$i];
		       $rres =   insert_record($XMLfilename_raw);                 

                   if($rres)
		   {                           
			  file_put_contents($log_path,"Record imported (biblio number) :".$rres."\n",FILE_APPEND);
		   }
		   else
		   {
                          file_put_contents($log_path,"Error in file :".$XMLfilename_raw."\n",FILE_APPEND);
		   }

                          $last_status['repository'] = $repository;
                          $last_status['recordCount'][$repository] =  $i;
 			   file_put_contents($last_import_timestamp_path, serialize($last_status));
 
               }
           }
       }

       function authenticate()
       {
	      exec("curl http://192.168.1.245:8080/cgi-bin/koha/svc/authentication -d 'userid=admin&password=Admin123' --cookie-jar /tmp/svc.cookies", $output);           
       }
 
      function insert_record($xmlfile){
	      global $retry;
              exec("curl 'http://192.168.1.245:8080/cgi-bin/koha/svc/new_bib?items=1' --cookie /tmp/svc.cookies --header 'Content-type: text/xml' --data @".$xmlfile, $output, $executed);
             if($output){ 
		        $output = implode('',$output);
			 $xml_array  = object2array(simplexml_load_string($output));
			

                         if(isset($xml_array['status']) && $xml_array['status'] == 'ok'){
                                 return $xml_array['biblionumber'];
                          }
			 elseif(isset($xml_array['auth_status']) && ($xml_array['auth_status'] == 'expired' || $xml_array['auth_status'] == 'failed')){
				   if($xml_array['auth_status'] == 'failed') $retry++;
				   if($retry == 4) return false;
				   authenticate();
				   insert_record($xmlfile);                                 
			 }
			 elseif(isset($xml_array['status']) && $xml_array['status']=='failed')
			 {
                              return false;
	                 }			 
		 }else {  
		         return false;
		 } 
  }
function object2array($object) { return @json_decode(@json_encode($object),1); }

?>
